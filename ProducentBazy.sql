CREATE TABLE Conferences (
	ConferenceID int IDENTITY(1,1) PRIMARY KEY,
	name varchar(255),
	studentsDiscount INT CHECK (studentsDiscount>0 and studentsDiscount<100)
);
CREATE TABLE ConferenceDays (
	DayID INT IDENTITY(1,1) PRIMARY KEY,
	ConferenceID INT FOREIGN KEY REFERENCES Conferences(ConferenceID),
	date DATE,
	participantsLimit INT,
	minParticipantsLimit INT, 
	price MONEY CHECK (price>=0),
	isCanceled BIT,
	cancelDate DATE,
	CONSTRAINT CK_participants CHECK (minParticipantsLimit<participantsLimit),
	CONSTRAINT CK_cancelDate CHECK (cancelDate<date)
); 
CREATE TABLE Discounts (
	DiscountID INT IDENTITY(1,1) PRIMARY KEY,
	DayID INT FOREIGN KEY REFERENCES ConferenceDays(DayID),
	numberDaysBefore INT,
	discount INT CHECK (discount>0 and discount<100)
);
CREATE TABLE Workshops (
	WorkshopID INT IDENTITY(1,1) PRIMARY KEY,
	DayID INT FOREIGN KEY REFERENCES ConferenceDays(DayID),
	name varchar(255),
	hourStart TIME,
	hourEnd TIME,
	ParticipantsLimit INT,
	minParticipantsLimit INT,
	price INT CHECK (price>=0),
	isCanceled BIT,
	cancelDate DATE,
	CONSTRAINT CK_participants2 CHECK (minParticipantsLimit<participantsLimit),
	CONSTRAINT CH_hour CHECK (hourEnd>hourStart)
);
CREATE TABLE Customers (
	CustomerID INT IDENTITY(1,1) PRIMARY KEY,
	phoneNumber VARCHAR(20),
	email VARCHAR(255) CHECK (email like '%@%')
);
CREATE TABLE Companies (
	CompanyID INT IDENTITY(1,1) PRIMARY KEY,
	CustomerID INT FOREIGN KEY REFERENCES Customers(CustomerID),
	name VARCHAR(255),
	NIP VARCHAR(255) CHECK (isnumeric(NIP)=1)
);
CREATE TABLE Private (
	PrivateID INT IDENTITY(1,1) PRIMARY KEY,
	CustomerID INT FOREIGN KEY REFERENCES Customers(CustomerID),
	firstName VARCHAR(255),
	lastName VARCHAR(255),
	pesel VARCHAR(255) CHECK (isnumeric(pesel)=1)
);
CREATE TABLE Reservations (
	ReservationID INT IDENTITY(1,1) PRIMARY KEY,
	CustomerID INT FOREIGN KEY REFERENCES Customers(CustomerID),
	reservationDate DATE,
	paid MONEY CHECK (paid>=0),
);
CREATE TABLE ReservationsConferences (
	ReservationID INT FOREIGN KEY REFERENCES Reservations(ReservationID),
	DayID INT FOREIGN KEY REFERENCES ConferenceDays(DayID),
	numberNormal INT,
	numberStudent INT,
	isCanceled bit,
	CONSTRAINT PK_ReservationsConferences PRIMARY KEY (ReservationID,DayID)
);
CREATE TABLE ReservationsWorkshops (
	ReservationID INT FOREIGN KEY REFERENCES Reservations(ReservationID),
	WorkshopID INT FOREIGN KEY REFERENCES Workshops(WorkshopID),
	numberNormal INT,
	numberStudent INT,
	isCanceled bit,
	CONSTRAINT PK_ReservationsWorkshops PRIMARY KEY (ReservationID,WorkshopID)
);
CREATE TABLE Persons (
	PersonID INT IDENTITY(1,1) PRIMARY KEY,
	ReservationID INT FOREIGN KEY REFERENCES Reservations(ReservationID),
	firstName VARCHAR(255),
	lastName VARCHAR(255)
);
CREATE TABLE PersonsConferences (
	PersonID INT FOREIGN KEY REFERENCES Persons(PersonID),
	DayID INT FOREIGN KEY REFERENCES ConferenceDays(DayID),
	isCanceled bit,
	CONSTRAINT PK_PersonsConferences PRIMARY KEY (PersonID,DayID)
);
CREATE TABLE PersonsWorkshops (
	PersonID INT FOREIGN KEY REFERENCES Persons(PersonID),
	WorkshopID INT FOREIGN KEY REFERENCES Workshops(WorkshopID),
	isCanceled bit,
	CONSTRAINT PK_PersonsWorkshops PRIMARY KEY (PersonID,WorkshopID)
);
CREATE TABLE Students (
	PersonID INT PRIMARY KEY FOREIGN KEY REFERENCES Persons(PersonID),
	studentID VARCHAR(255)
);